﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerScript_ex00 : MonoBehaviour
{
    public float    maxSpeed = 5f;

	private void Start()
    {}

	private void FixedUpdate()
    {
        float move = Input.GetAxis("Horizontal");

        GetComponent<Rigidbody2D>().velocity = new Vector2(move * maxSpeed,
            GetComponent<Rigidbody2D>().velocity.y);
    }
}
